var muovi = true;
var verticale = 0;
var orizzontale = 0;
var latitudine = 0;
var longitudine = 0;

function centra() {
    var palla = document.getElementById("palla");
    var larghezzaPalla = palla.width;
    var altezzaPalla = palla.height;
    var larghezzaFinestra = window.innerWidth;
    var altezzaFinestra = window.innerHeight;
    palla.style.position = 'absolute';
    palla.style.left = (larghezzaFinestra / 2 - larghezzaPalla / 2) + 'px';
    palla.style.top = (altezzaFinestra / 2 - altezzaPalla / 2) + 'px';
}
function sposta() {
    var palla = document.getElementById("palla");
    var larghezzaPalla = palla.width;
    var altezzaPalla = palla.height;
    var larghezzaFinestra = window.innerWidth;
    var altezzaFinestra = window.innerHeight;
    muovi = false;
    palla.style.left = (larghezzaFinestra / 2 - larghezzaPalla / 2) + orizzontale + 'px';
    palla.style.top = (altezzaFinestra / 2 - altezzaPalla / 2) + verticale + 'px';
    if (((altezzaFinestra / 2 + altezzaPalla / 2) + verticale) < altezzaFinestra && latitudine % 2 == 0) {
        verticale += 1;
    }
    if (((altezzaFinestra / 2 + altezzaPalla / 2) + verticale) >= altezzaFinestra && latitudine % 2 == 0) {
        latitudine++;
        verticale -= 1;
    }
    if (((altezzaFinestra / 2 - altezzaPalla / 2) + verticale) < altezzaFinestra && latitudine % 2 != 0) {
        verticale -= 1;
    }
    if (((altezzaFinestra / 2 - altezzaPalla / 2) + verticale) < 0 && latitudine % 2 != 0) {
        latitudine++;
        verticale += 1;
    }
    if (((larghezzaFinestra / 2 + larghezzaPalla / 2) + orizzontale) < larghezzaFinestra && longitudine % 2 == 0) {
        orizzontale += 1;
    }
    if (((larghezzaFinestra / 2 + larghezzaPalla / 2) + orizzontale) >= larghezzaFinestra && longitudine % 2 == 0) {
        longitudine++;
        orizzontale -= 1;
    }
    if (((larghezzaFinestra / 2 - larghezzaPalla / 2) + orizzontale) < larghezzaFinestra && longitudine % 2 != 0) {
        orizzontale -= 1;
    }
    if (((larghezzaFinestra / 2 - larghezzaPalla / 2) + orizzontale) < 0 && longitudine % 2 != 0) {
        longitudine++;
        orizzontale += 1;
    }
}
setInterval(() => {
    if (muovi) {
        centra();
    } else {
        sposta();
    }
}, 1);