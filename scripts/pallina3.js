// Dichiarazione dell'oggetto
var dimensioniFinestra = {
    larghezza: window.innerWidth,
    altezza: window.innerHeight
}

var body = document.getElementsByTagName('body')[0];

var palline = [];

var timer;

function generaPallina() {

    var pallina = document.createElement('img');
    pallina.src = 'immagini/pallina2.png';
    pallina.dim = 50;
    pallina.coordinate = {
        x: Math.random() * (dimensioniFinestra.larghezza - pallina.dim),
        y: Math.random() * (dimensioniFinestra.altezza - pallina.dim)
    }
    pallina.direzione = {
        h: Math.random() > 0.5 ? 0 : 1,
        v: Math.random() > 0.5 ? 0 : 1
    }
    pallina.style.position = 'absolute';
    pallina.style.left = pallina.coordinate.x + 'px';
    pallina.style.top = pallina.coordinate.y + 'px';

    body.appendChild(pallina);

    return pallina;

    
}
function generaPalline() {
    var nrPalline = document.getElementById('nrPalline').value;
    console.log(nrPalline);
    for (let i = 0; i < nrPalline; i++) {
        var pallina = generaPallina();
        palline.push(pallina)
    }
}

function avvia() {
    console.log(palline);
    timer = setInterval(() => {
        for (let index = 0; index < palline.length; index++) {
         if (palline[index].direzione.h == 0 && palline[index].coordinate.x < (dimensioniFinestra.larghezza - palline[index].dim)) {
             palline[index].coordinate.x += 1;
         } else {
             palline[index].coordinate.x -= 1;
            palline[index].direzione.h = 1;
         }
         if (palline[index].direzione.h == 1 && palline[index].coordinate.x > 0) {
             palline[index].coordinate.x -= 1;
         } else {
             palline[index].coordinate.x += 1;
            palline[index].direzione.h = 0;
         }
         if (palline[index].direzione.v == 0 && palline[index].coordinate.y < (dimensioniFinestra.altezza - palline[index].dim)) {
             palline[index].coordinate.y += 1;
         } else {
             palline[index].coordinate.y -= 1;
            palline[index].direzione.v = 1;
         }
         if (palline[index].direzione.v == 1 && palline[index].coordinate.y > 0) {
             palline[index].coordinate.y -= 1;
         } else {
             palline[index].coordinate.y += 1;
            palline[index].direzione.v = 0;
         }
    palline[index].style.left = palline[index].coordinate.x + 'px';
    palline[index].style.top = palline[index].coordinate.y + 'px';
         
    }
    }, 10);
    
}
function ferma() {
    clearInterval(timer);
}