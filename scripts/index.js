function concetti1() {
    alert("Orologio:\n• Collegamento del file JavaScript a HTML;\n• Apprendere il significato di document.getElementById();\n• Utilizzo di setTimeout e setInterval.")
}
function concetti2() {
    alert("Lampada:\n• Utilizzo di onclick per la sostituzione dell'immagine della lampada;\n• Sapere come si centra un'immagine;\n• Nascondere l'immagine dopo un tot di tempo.")
}
function concetti31() {
    alert("Pallina singola:\n• Riuscire a muovere la pallina al momento del click con il mouse.")
}
function concetti32() {
    alert("Palline casuali:\n• Utilizzo di onclick per la sostituzione dell'immagine della lampada;\n• Sapere come si centra un'immagine;\n• Nascondere l'immagine dopo un tot di tempo.")
}
function concetti33() {
    alert("Generatore di palline:\n• Introduzione dei vettori;\n• Dichiarazione dell'oggetto.")
}
function concetti4() {
    alert("Oggetti:\n• Introduzione degli oggetti;\n• Utilizzare HTML nel file JavaScript.")
}
function concetti5() {
    alert("File JSON:\n• Introduzione dei file JSON;\n• Utilizzare dei metodi per trasformare i dati JSON nel file JavaScript.")
}