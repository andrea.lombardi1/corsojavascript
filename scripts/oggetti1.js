var persone = [ {
    // Nome attributo: valore, ...
    nome: 'Andrea',
    cognome: 'Lombardi',
    dataNascita: new Date('05/10/2004'),
    telefono: '1122334455',
    mail: 'andrea.lombardi@marconirovereto.it'
}, {
    nome: 'Veronica',
    cognome: 'Lombardi',
    dataNascita: new Date('12/08/2002'),
    telefono: '5566778899',
    mail: 'veronica.lombardi@g-floriani.it'
}, {
    nome: 'Gianfranco',
    cognome: 'Lombardi',
    dataNascita: new Date('09/11/1966'),
    telefono: '1122334455',
    mail: 'www.lombrico4@gmail.com'
}, {
    nome: 'Alessandra',
    cognome: 'Brighenti',
    dataNascita: new Date('02/06/1973'),
    telefono: '5566778899',
    mail: 'lombrico4@yahoo.it'
} ]

function intestazione() {
    return '<tr>' +
    '    <th>Nome</th>' +
    '    <th>Cognome</th>' +
    '    <th>Data di nascita</th>' +
    '    <th>Telefono</th>' +
    '    <th>Mail</th>' +
    '</tr>';
}

function riga(persona) {
    return '<tr>' +
    '    <td>' + persona.nome + '</td>' +
    '    <td>' + persona.cognome + '</td>' +
    '    <td>' + persona.dataNascita.toLocaleDateString() + '</td>' +
    '    <td>' + persona.telefono + '</td>' +
    '    <td>' + persona.mail + '</td>' +
    '</tr>';
}

function tabella(persone) {
    return '<table border="1">' +
        intestazione() +
        corpo(persone) +
    '</table>';
}

function corpo(persone) {
    var codiceHTML = '';
    persone.forEach(persona => {
        codiceHTML += riga(persona);
    });
    return codiceHTML;
}

var main = document.getElementById('main');
main.innerHTML = tabella(persone);
console.log(persone);