var accesa = false;

function init() {
    var immagini = [];
    immagini[0] = new Image();
    immagini[0].src = "immagini/spenta.jpg";
    immagini[1] = new Image();
    immagini[1].src = "immagini/accesa.jpg";
}

function accendi() {
    var lampada = document.getElementById("lampada");
    if (accesa) {
        lampada.src = "immagini/spenta.jpg";
        accesa = false;
    } else {
        lampada.src = "immagini/accesa.jpg";
        accesa = true;
    }
}

function centra() {
    var larghezzaFinestra = window.innerWidth;
    var altezzaFinestra = window.innerHeight;

    var lampada = document.getElementById("lampada");
    var larghezzaLampada = lampada.width;
    var altezzaLampada = lampada.height;

    lampada.style.position = 'absolute';
    lampada.style.left = (larghezzaFinestra / 2 - larghezzaLampada / 2) + 'px';
    lampada.style.top = (altezzaFinestra / 2 - altezzaLampada / 2) + 'px';
}
init();
setInterval(() => {
centra();
}, 1);
setTimeout(() => {
    
    var lampada = document.getElementById("lampada");
    lampada.style.visibility = 'hidden';
}, 10000);