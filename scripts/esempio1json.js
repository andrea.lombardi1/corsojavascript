var client = new XMLHttpRequest();

var persone;

function creaIntestazione() {
    var intestazione = document.createElement('tr');
    var cella = document.createElement('th');
    cella.innerText = 'id';
    intestazione.appendChild(cella);
    
    cella = document.createElement('th');
    cella.innerText = 'Nome';
    intestazione.appendChild(cella);
    cella = document.createElement('th');
    cella.innerText = 'Cognome';
    intestazione.appendChild(cella);
    cella = document.createElement('th');
    cella.innerText = 'Mail';
    intestazione.appendChild(cella);
    cella = document.createElement('th');
    cella.innerText = 'Sesso';
    intestazione.appendChild(cella);
    cella = document.createElement('th');
    cella.innerText = 'IP';
    intestazione.appendChild(cella);
    
    return intestazione;
}

function creaRiga(persona) {
    var intestazione = document.createElement('tr');
    var cella = document.createElement('th');
    cella.innerText = persona.id;
    intestazione.appendChild(cella);
    
    cella = document.createElement('td');
    cella.innerText = persona.first_name;
    intestazione.appendChild(cella);
    cella = document.createElement('td');
    cella.innerText = persona.last_name;
    intestazione.appendChild(cella);
    cella = document.createElement('td');
    cella.innerText = persona.email;
    intestazione.appendChild(cella);
    cella = document.createElement('td');
    cella.innerText = persona.gender;
    intestazione.appendChild(cella);
    cella = document.createElement('td');
    cella.innerText = persona.ip_address;
    intestazione.appendChild(cella);
    
    return intestazione;
}

function creaTabella(persone) {
    var tabella = document.createElement('table');
    tabella.border = 1;
    tabella.appendChild(creaIntestazione());

    persone.forEach(persona => {
        tabella.appendChild(creaRiga(persona));
    });

    return tabella;
}

function loadData() {
    client.onreadystatechange = () => {
        // Codice che viene eseguito quando il server che ospita la risorsa richiesta
        // mi manda una risposta
    
        // Controllo il tipo di risposta
    
        if (client.readyState == 4 && client.status == 200) {
            // Bingo!!! i dati sono arrivati e sono a disposizione
    
            // Visulizzo i dati in formato JSON
            console.log('JSON:', client.response);
    
            //Converto i dati in formato javascript
            persone = JSON.parse(client.response);
    
            // Visualizza gli stessi dati in formato javascript
            console.log('Javascript:', persone);
    
            // Crea la tabella e la visualizza
            
            var divMain = document.getElementById('main');
            divMain.appendChild(creaTabella(persone));    
        }
    }
    
    client.open('GET', './datipersone.json');
    client.send();
}