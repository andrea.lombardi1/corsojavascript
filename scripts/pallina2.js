var pallina1 = document.getElementById("pallina1");
var pallina2 = document.getElementById("pallina2");
var pallina3 = document.getElementById("pallina3");
var dimPallina = 50;
var larghezza = window.innerWidth;
var altezza = window.innerHeight;
var posX1 = Math.floor(Math.random() * larghezza - dimPallina);
var posY1 = Math.floor(Math.random() * altezza - dimPallina);
var posX2 = Math.floor(Math.random() * larghezza - dimPallina);
var posY2 = Math.floor(Math.random() * altezza - dimPallina);
var posX3 = Math.floor(Math.random() * larghezza - dimPallina);
var posY3 = Math.floor(Math.random() * altezza - dimPallina);
var dirOrizz1 = "dx";
var dirVert1 = "giu";
var dirOrizz2 = "sx";
var dirVert2 = "giu";
var dirOrizz3 = "sx";
var dirVert3 = "su";


pallina1.style.position = "absolute";
pallina1.style.top = posX1 + "px";
pallina1.style.left = posY1 + "px";
pallina2.style.position = "absolute";
pallina2.style.top = posX2 + "px";
pallina2.style.left = posY2 + "px";
pallina3.style.position = "absolute";
pallina3.style.top = posX3 + "px";
pallina3.style.left = posY3 + "px";

function resize() {
    larghezza = window.innerWidth;
    altezza = window.innerHeight;
}

setInterval(() => {
    if (dirOrizz1 == "dx") {
        if (posX1 >= larghezza - dimPallina) {
            dirOrizz1 = "sx";
        }
    }
    else {
        if (posX1 <= 0) {
            dirOrizz1 = "dx";
        }
    }

    if (dirVert1 == "giu") {
        if (posY1 >= altezza - dimPallina) {
            dirVert1 = "su";
        }
    }
    else {
        if (posY1 <= 0) {
            dirVert1 = "giu";
        }
    }

    if (dirOrizz1 == "dx") {
        posX1++;
    }
    else {
        posX1--;
    }
    if (dirVert1 == "giu") {
        posY1++;
    }
    else {
        posY1--;
    }

    pallina1.style.top = posY1 + "px";
    pallina1.style.left = posX1 + "px";
}, 1);

setInterval(() => {

    // Eseguo questo codice ogni 5 ms

    if (dirOrizz2 == "dx") {
        if (posX2 >= larghezza - dimPallina) {
            dirOrizz2 = "sx";
        }
    }
    else {
        if (posX2 <= 0) {
            dirOrizz2 = "dx";
        }
    }

    if (dirVert2 == "giu") {
        if (posY2 >= altezza - dimPallina) {
            dirVert2 = "su";
        }
    }
    else {
        if (posY2 <= 0) {
            dirVert2 = "giu";
        }
    }

    if (dirOrizz2 == "dx") {
        posX2++;
    }
    else {
        posX2--;
    }
    if (dirVert2 == "giu") {
        posY2++;
    }
    else {
        posY2--;
    }


    pallina2.style.top = posY2 + "px";
    pallina2.style.left = posX2 + "px";

}, 5);

setInterval(() => {
    if (dirOrizz3 == "dx") {
        if (posX3 >= larghezza - dimPallina) {
            dirOrizz3 = "sx";
        }
    }
    else {
        if (posX3 <= 0) {
            dirOrizz3 = "dx";
        }
    }

    if (dirVert3 == "giu") {
        if (posY3 >= altezza - dimPallina) {
            dirVert3 = "su";
        }
    }
    else {
        if (posY3 <= 0) {
            dirVert3 = "giu";
        }
    }

    if (dirOrizz3 == "dx") {
        posX3++;
    }
    else {
        posX3--;
    }
    if (dirVert3 == "giu") {
        posY3++;
    }
    else {
        posY3--;
    }
    pallina3.style.top = posY3 + "px";
    pallina3.style.left = posX3 + "px";
}, 10);