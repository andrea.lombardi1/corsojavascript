//alert("Hello world!");
var elemento;
elemento = document.getElementById("elemento");

//Di solito si usa la virgoletta singola ' ma potrebbe essere usata tranquillamente la " 
elemento.innerText = 'Questa è una figata';

var orario= new Date();
document.getElementById("orainiziale").innerText = orario.toLocaleString();

//l'orologio mostrato sulla pagina, deve essere aggiornato ogni secondo

//Occhio! Chiamate ASINCRONE delle funzioni con sintassi LAMBDA
//nelle parentesi () posso elencare dei parametri
setTimeout(() => {
  //codice da eseguire
  orario = new Date();
  document.getElementById("oraritardata").innerText = orario.toLocaleString();

}, 5000); //questo codice verrà eseguito DOPO 5000 ms

/*
//Equivalente senza sintassi LAMBDA
setTimeout(function() {

});
*/

//chiamate ASINCRONE
//Il codice viene eseguito in modo asincrono ogni secondo
setInterval(() => {
  //codice da eseguire
  orario = new Date();
  document.getElementById("orologio").innerText = orario.toLocaleString();
}, 1000);

alert("ciao!");

